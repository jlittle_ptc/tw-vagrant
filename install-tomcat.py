# Imports
import argparse
import requests
import re
import wget
import tempfile
import os.path
from bs4 import BeautifulSoup

# Constants

# Tomcat's mirrors only host the latest version of each branch.  If we want a 
# previous version, we need to use the apache archive site.
MIRRORS = ["http://ftp.wayne.edu/apache/tomcat/"]
ARCHIVE = ["http://archive.apache.org/dist/tomcat/"]

# Supported Major and Major/Minor combinations.
MAJOR_VERSIONS = [7, 8, 9]
MINOR_VERSIONS = {7: [0], 8: [5, 0], 9: [0]}

TEMPDIR = "/tmp"

def parse_version(verStr):
    if verStr is None:
        return None
    
    if "v" in verStr:
        # This is a version from apache's directory listing.  Strip off first and last characters.
        verStr = verStr[1:-1]

    s = verStr.split(".")
    
    try:
        if len(s) == 1:
            # Major only.
            if int(s[0]) in MAJOR_VERSIONS:
                return {'major': int(s[0]), 'minor': MINOR_VERSIONS[int(s[0])][0], 'patch': None}
        elif len(s) == 2:
            # Major, minor
            if int(s[0]) in MAJOR_VERSIONS and int(s[1]) in MINOR_VERSIONS[int(s[0])]:
                return {'major': int(s[0]), 'minor': int(s[1]), 'patch': None}
        elif len(s) == 3:
            if int(s[0]) in MAJOR_VERSIONS and int(s[1]) in MINOR_VERSIONS[int(s[0])]:
                return {'major': int(s[0]), 'minor': int(s[1]), 'patch': int(s[2])}

    except ValueError:
        # Just fall out, we'll log an error next.
        pass
    
    print("Unable to parse '%s' as a version string." % (verStr, ))
    return None


def build_arg_parser():
    parser = argparse.ArgumentParser(description="Downloads the Apache Tomcat distribution for a specific release.")
    parser.add_argument("version", type=str, help="Version to download in Major, Major.Minor or Major.Minor.Patch format.")
    return parser


def download_latest(major, minor):
    # We can download from a mirror
    for m in MIRRORS:
        url = m + "tomcat-" + str(major) + "/"
        print("Retrieving latest version of Tomcat from %s" % (url, ))
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        anchors = soup("a", href=re.compile("v"))
        
        if len(anchors) == 0:
            # No versions found, so maybe the mirror is bad? Continue with next mirror.
            continue

        if len(anchors) == 1:
            # Only a single version found (latest)
            ver = parse_version(anchors[0]['href'])
            url += "%sbin/apache-tomcat-%d.%d.%d.zip" % (anchors[0]['href'], ver['major'], ver['minor'], ver['patch'])
            break
        
        else:
            # Multiple possible links, figure out latest.
            links = []
            for a in anchors:
                v = parse_version(a['href'])
                if v is not None and v['major'] == major and v['minor'] == minor:
                    links.append(v['patch'])

            latest = sorted(links, reverse=True)[0]
            ver = {'major': major, 'minor': minor, 'patch': latest}
            url += "v%d.%d.%d/bin/apache-tomcat-%d.%d.%d.zip" % (major, minor, latest, major, minor, latest)
            break

    return download_file(url)


def download_archive(major, minor, patch):
    if patch is None:
        # Asking for latest, so just use other method.
        return download_latest(major, minor)
    
    # We have to download from archive.
    for m in ARCHIVE:
        url = m + "tomcat-" + str(major) + "/"
        print("Retrieving specific version of Tomcat (%d.%d.%d) from %s" % (major, minor, patch, url))
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        anchors = soup("a", href=re.compile("v\.%d\.%d\.%d" % (major, minor, patch)))
        
        if len(anchors) == 0:
            # No versions found, so maybe the mirror is bad? Continue with next mirror.
            continue

        if len(anchors) == 1:
            # Only a single version found (latest)
            ver = parse_version(anchors[0]['href'])
            url += "%sbin/apache-tomcat-%d.%d.%d.zip" % (anchors[0]['href'], ver['major'], ver['minor'], ver['patch'])
            break
        
        #else:
            # # Multiple possible links, figure out latest.
            # links = []
            # for a in anchors:
            #     v = parse_version(a['href'])
            #     if v is not None and v['major'] == major and v['minor'] == minor:
            #         links.append(v['patch'])

            # latest = sorted(links, reverse=True)[0]
            # ver = {'major': major, 'minor': minor, 'patch': latest}
            # url += "v%d.%d.%d/bin/apache-tomcat-%d.%d.%d.zip" % (major, minor, latest, major, minor, latest)
            # break

    return download_file(url)


def download_file(url):
    filename = url[url.rfind("/") + 1:]
    tempPath = tempfile.gettempdir() + "/" + filename

    # Make sure the file doesn't already exist...
    if os.path.exists(tempPath):
        print("Deleting existing temp file at %s" % (tempPath, ))
        os.remove(tempPath)

    print("Attempting to download file from: %s" % (url, ))
    wget.download(url, tempPath)
    
    print()
    print("Download complete!")

    return tempPath




# Main
parser = build_arg_parser()
args = parser.parse_args()

download_archive(8,5,20)
