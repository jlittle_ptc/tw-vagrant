#!/usr/bin/env python3

# Imports
import os
import sys
import platform
import subprocess
import tarfile

def isRoot():
    if os.getenv("SUDO_USER") is None:
        return False
    else:
        return True


def getJavaVersion():
    toReturn = {'ver': -1, 'dist': "Unknown", 'dir': ""}

    if platform.system() == "Linux":
        result = subprocess.run("which java", shell=True, stdout=subprocess.PIPE)

        if result.returncode != 0:
            # Java is not installed, or some other error occurred
            print("getJavaVersion(): Unable to find local Java install.")
            return None
        else:
            toReturn['dir'] = result.stdout.decode('UTF-8').strip()

        # Check version - Output is written to stderr, not stdout.
        javaResult = subprocess.run("java -version", shell=True, stderr = subprocess.PIPE)

        # Oracle Java 8:
        #   java version "1.8.0_231"
        #   Java(TM) SE Runtime Environment (build 1.8.0_231-b11)

        # Amazon Java 8:
        #    openjdk version "1.8.0_232"
        #    OpenJDK Runtime Environment Corretto-8.232.09.1 (build 1.8.0_232-b09)

        # Oracle Java 11:
        #   java version "11" 2018-09-25
        #   Java(TM) SE Runtime Environment 18.9 (build 11+28)

        # Amazon Java 11:

        # OpenJDK Java 11:
        #    openjdk version "11" 2018-09-25
        #    OpenJDK Runtime Environment 18.9 (build 11+28)

        v = javaResult.stderr.decode('UTF-8').split('\n')

        # Only 8 and 11 have been verified.  Rest are guesses at this time, but
        # likely to be unsupported anyway as they are not LTS releases
        versions = {8: "1.8.0", 9: '"9"', 10: '"10"', 11: '"11"', 12: '"12"', \
            13: '"13"', 14: '"14"'}

        # Version
        for key, val in versions.items():
            if val in v[0]:
                toReturn['ver'] = key
                break

        # Distribution
        if "Corretto" in v[1]:
            toReturn['dist'] = "Corretto"
        elif "Java(TM) SE" in v[1]:
            toReturn['dist'] = "Oracle"
        elif "OpenJDK" in v[1]:
            toReturn['dist'] = "OpenJDK"

        return toReturn

    elif platform.system() == "Windows":
        print("Windows support not yet implemented.")
        exit(1)


def install_java_dist(javaDistFile, targetDir="/usr/lib/jvm", verbose=False):
    # Is Java already installed?
    version = getJavaVersion()
    if version:
        print("%s Java version %d is already installed at '%s'.  Will not install." % (version['dist'], version['ver'], version['dir']))
        return False

    # Check Parameters
    if not javaDistFile:
        print("Path to Java Distribution not specified. Cannot install.")
        return False

    if not targetDir:
        print("Target Installation Directory not specified.  Cannot install.")
        return False

    # Check if root
    if not isRoot():
        print("This install script must be run as root. Cannot install.")
        return False

    # Validate distfile
    if not os.path.isfile(javaDistFile):
        print("Path to Java Distribution '%s' does not exist. Cannot install." % (javaDistFile, ))
        return False
    elif not javaDistFile.endswith(".tar.gz"):
        print("Java Distribution file '%s' does not appear to be a .tar.gz file." % (javaDistFile, ))
        return False

    # Create target directory if necessary
    if os.path.exists(targetDir) and os.path.isdir(targetDir) and len(os.listdir(targetDir)) > 0:
        print("Target install dir '%s' exists and is not an empty directory.  Will not install." % (javaDistFile, ))
    else:
        print("Creating target install directory: %s" % (targetDir, ))
        os.makedirs(targetDir, exist_ok=True)

    ### Install java

    # Open the tarfile.
    try:
        tar = tarfile.open(javaDistFile, "r:gz")

        # Make sure the tar file contains a java bin file.
        javadirname = None
        found = False
        for tarinfo in tar:
            if tarinfo.name.endswith("/bin/java"):
                javadirname = tarinfo.name.split("/")[0]
                found = True
                break

        if not found:
            print("Unable to find bin/java file in Java distribution.")
            return False

        # Extract archive to target location.
        print("Extracting Java Distribution to %s..." % (targetDir, ))
        tar.extractall(targetDir)

    except tarfile.ReadError:
        print("Unable to read Java Distribution file.")
    finally:
        if tar is not None:
            tar.close()

    # Update Alternatives
    javadir = targetDir + "/" + javadirname
    run_update_alternatives(javadir)

    # Set JAVA_HOME
    set_java_home(javadir)


def run_update_alternatives(javaDir, verbose = False):
    print("Running update-alternatives...")
    binFiles = os.listdir(javaDir + "/bin")

    commands = []

    for binfile in binFiles:
        cmd_tgt = javaDir + "/bin/" + binfile
        cmd1 = 'update-alternatives --install "/usr/bin/%s" "%s" "%s" 10000' % (binfile, binfile, cmd_tgt)
        cmd2 = 'update-alternatives --set "%s" "%s"' % (binfile, cmd_tgt)
        if os.path.exists(cmd_tgt):
            if verbose:
                print("Running command: %s" % (cmd1, ))
            subprocess.run(cmd1, shell=True, check=True)
            if verbose:
                print("Running command: %s" % (cmd2, ))
            subprocess.run(cmd2, shell=True, check=True)
        else:
            print("Unable to run update-alternatives for %s" % (cmd_tgt, ))


def set_java_home(javaDir):
    homeDir = os.getenv("HOME")

    if homeDir is None:
        return

    rc = homeDir + "/.bashrc"
    # Check if .bashrc already has a JAVA_HOME parameter.
    if os.path.exists(rc):
        r = subprocess.run('grep -q "export JAVA_HOME" ' + rc, shell=True, check=False)
        if r.returncode == 0:
            print(".bashrc already has a JAVA_HOME parameter and will not be updated.")
            return

    print("Updating .bashrc to add JAVA_HOME...")
    with open(rc, "w+") as f:
        f.write("export JAVA_HOME=" + javaDir + "\n")


def display_help():
    print("Usage:")
    print("  sudo ./install-java.py <pathToJavaDist.tar.gz>")


# Main
if len(sys.argv) == 1 or len(sys.argv) > 2:
    display_help()
else:
    install_java_dist(sys.argv[1])
