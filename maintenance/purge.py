#!/usr/bin/env python3

# Imports
import os
import subprocess
import platform

# Check if Ubuntu
if platform.system() is not "Linux" and "Ubuntu" not in platform.version():
    print("This script must be run on an Ubuntu distribution. Platform found: " + platform.platform())
    exit(1)

# Check if root
if os.getenv("SUDO_USER") is None:
    print("This script must be run as root.")
    exit(1)

# Clean APT repository
print("Cleaning out the APT repository...")
subprocess.run("apt-get -y autoremove", shell=True, check=True)
subprocess.run("apt-get clean", shell=True, check=True)
subprocess.run("apt-get autoclean", shell=True, check=True)

# Clean bash history
print("Cleaning up bash history...")
subprocess.run("unset HISTFILE", shell=True, check=True)

if os.path.exists("/root/.bash_history"):
    os.remove("/root/.bash_history")

if os.path.exists("/home/vagrant/.bash_history"):
    os.remove("/home/vagrant/.bash_history")

# Clean log files
print("Cleaning up log files...")
logfiles = os.listdir("/var/log")
for name in logfiles:
    if os.path.isfile(name):
        os.remove(name)

# Zero out free space
print("Zeroing out free space... (This could take a few minutes)")

# dd will return a non-zero exit code (Out of space) which is expected, so we
# want to ignore this.
subprocess.run("dd if=/dev/zero of=/EMPTY bs=1M", shell=True, check=False)
os.remove("/EMPTY")
